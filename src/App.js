import React, { Component } from 'react';
import './style-screen.css';
import Modal from "./Modal";

class App extends Component {

  constructor(props)
  {
    super(props);

    this.state = {
        value: '',
        information: [],
        hidden: true,
        updated: false,
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleSubmit = (event) =>
  {
      event.preventDefault();

      if(this.state.value !== '')
      {
        fetch("https://jsonplaceholder.typicode.com/posts/" + this.state.value)
            .then((response) => {
                return response.json();
            })
            .then( (myJson) => {
                console.log(myJson);

                (Object.keys(myJson).length === 0) ? alert("No se encontró") : this.setState({information: myJson, hidden: false, updated: false});
            })
      }
      else
      {
        alert("No puede estar vacio.");
      }
  }

  handleChange = (event) =>
  {
      this.setState({value: event.target.value});
  }

  changeHidden= () =>
  {
      this.setState({hidden: true});
  }

  saveInfo= (e) =>
  {
    console.log(e);
    let {information} = this.state;

    information.title = e.title;
    information.body = e.body;
    information.time = e.date.getMonth()+"/"+e.date.getDate()+"/"+e.date.getFullYear()+" " +this.formatTime(e.date.getHours(),e.date.getMinutes(),e.date.getSeconds());
    //this.state.information(info => console.log(info));
    //console.log(information);
    this.setState({information, updated: true, hidden: true})

  }

  formatTime = (hours,minutes,seconds) =>
  {
    hours = (hours<10 ?'0':'') + hours;
    minutes = (minutes<10 ?'0':'') + minutes;
    seconds = (seconds<10 ?'0':'') + seconds;

    return hours + ":" + minutes + ":" + seconds;
  }
  render() {
    return (
        <div>
          <div className="wrapper-app container">
            <div className="form-cont">
                <form onSubmit={this.handleSubmit}>
                <label>Post ID</label>
                <input type="text" name="" placeholder="17" onChange={this.handleChange}/>
                <button className="btn-azul">Edit</button>

                <div className="clearfix"></div>
                </form>

                {this.state.updated && 
                  <div>
                    <h2>Title</h2>
                    <p>{this.state.information.title}</p>

                    <div className="clearfix"></div>

                    <h2>Body</h2>
                    <p>{this.state.information.body}</p>
                    <div className="clearfix"></div>

                    <h2>Time</h2>
                    <p>{this.state.information.time}</p>
                  </div>
                }

            </div>
          </div>
            <div className="clearfix" />
            {!this.state.hidden && <Modal {...this.state.information} hidden={this.changeHidden} updateInformation={this.saveInfo}/>}
        </div>
    );
  }
}

export default App;
