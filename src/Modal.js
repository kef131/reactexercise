import React, {Component} from 'react';

export default class Modal extends Component{


    constructor()
    {
        super();

        this.state = 
        {
            title: "",
            body: "",
            date: null,
        }
        this.onOk = this.onOk.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.handleTitle = this.handleTitle.bind(this);
        this.handleBody = this.handleBody.bind(this);
    }

    onOk = (event) =>
    {
        event.preventDefault();

        //Validation
        let {title,body,date} = this.state;
        title = (title==='') ? this.props.title : title;
        body = (body==='') ? this.props.body : body;
        date = (date===null) ? new Date() : date;
        this.props.updateInformation({title,body,date});
    }

    onCancel = () =>
    {
        this.props.hidden(false);
    }

    handleTitle = (event) =>
    {
        let title = event.currentTarget.value;
        this.setState({title, date: new Date()});
    }
    handleBody = (event) =>
    {
        let body = event.currentTarget.value;
        this.setState({body, date: new Date()});
    }

    render() {
        
        return (
            <div className="blackback">
            <div className="fancy-form container">
                <form>
                <button className="btn-cerrar" onClick={this.onCancel}>X</button>
                <h2>Editing: {this.props.id}</h2>
                <label>Title</label>
                <input defaultValue={this.props.title} onChange={this.handleTitle}/>
                <div className="clearfix" />
                <label>Body</label>
                <textarea defaultValue={this.props.body} onChange={this.handleBody}></textarea>
                <div className="clearfix" />
                <button className="btn-azul" onClick={this.onOk}>Save</button>
                <button className="btn-gris" onClick={this.onCancel}>Cancel</button>
                </form>
            </div>
            </div>
        );
    }
}